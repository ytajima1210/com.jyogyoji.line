<?php
	require_once(dirname(__FILE__). '/config.php');

	// Webfook用ログファイル
	$contents = file_get_contents(WEBHOOK_LOGFILE_PATH, false);

	// LINE ユーザIDを取得
	$raw     = file_get_contents('php://input');
	$receive = json_decode($raw, true);
	$type    = $receive['events'][0]['type'];
	// フォロー時のみ処理する
	if ($type === 'follow') {
		$contents .= date('Y/m/d H:i:s'). ' フォロー処理につきLINE ID登録開始'. PHP_EOL;
		// ユーザID取得
		$userId  = $receive['events'][0]['source']['userId'];
		$contents .= date('Y/m/d H:i:s'). ' 登録対象ID：'. $userId. PHP_EOL;
		// PDOオブジェクト取得
		$pdoObj = setDatabaseObject();
		// 登録済みLINE IDかどうかチェック
		$isRegist = existLineId($pdoObj, $userId);
		if ($isRegist) {
			// 未登録のためDBにLINE IDを登録
			registLineId($pdoObj, $userId);
			$contents .= date('Y/m/d H:i:s'). ' LINE ID登録終了'. PHP_EOL;
		} 
		else {
			$contents .= date('Y/m/d H:i:s'). ' LINE ID登録済みのため処理終了'. PHP_EOL;
		}
	}
	else {
		$contents .= date('Y/m/d H:i:s'). ' フォロー処理ではないので処理終了'. PHP_EOL;
	}

	// ログファイル更新して終了
	file_put_contents(WEBHOOK_LOGFILE_PATH, $contents);
	exit;

	/**
	 * データベース接続用PDOオブジェクトの用意
	 *
	 * @param		none
	 * @return		Object
	 */
	function setDatabaseObject() {
		// DB設定
		$mysql   = MYSQL_DBNAME;
		$user    = MYSQL_LOGIN_USER;
		$pass    = MYSQL_LOGIN_PASSWORD;
		$options = array(
			PDO::ATTR_ERRMODE             => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE  => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES    => false,
			PDO::ATTR_STRINGIFY_FETCHES   => false);
		$pdoObj = new PDO($mysql, $user, $pass, $options);
		$pdoObj->query('SET NAMES utf8');
		return $pdoObj;
	}

	/**
	 * すでに登録済みLINE IDかどうかチェックする
	 *
	 * @param		Object 		$pdoObj
	 * @param 		string 		$lineId
	 * @return 		boolean
	 */
	function existLineId($pdoObj, $lineId) {
		// パラメータで指定されたLINE IDが存在するかどうかチェック
		$state = $pdoObj->prepare('
			SELECT 
				line_id
			FROM 
				line_id_list 
			WHERE 
				line_id = :id ');
		$state->bindParam(':id', $lineId, PDO::PARAM_STR);
		$state->execute();
		$data = $state->fetch();

		return ($data === false);
	}

	/**
	 * 指定のLINE IDをデータベースに登録する
	 *
	 * @param		Object 		$pdoObj
	 * @param 		string 		$lineId
	 * @return 		void
	 */
	function registLineId($pdoObj, $lineId) {
		// パラメータで指定されたLINE IDが存在するかどうかチェック
		$state = $pdoObj->prepare('
			INSERT INTO 
				line_id_list 
			SET
				line_id = :id ');
		$state->bindParam(':id', $lineId, PDO::PARAM_STR);
		$state->execute();
	}


?>