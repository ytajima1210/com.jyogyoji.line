<?php
	require_once(dirname(__FILE__). '/config.php');
	require_once(dirname(__FILE__). '/class/LineAction.php');

	try {
		// Webfook用ログファイル
		$file     = sprintf(SENDLINE_LOGFILE_PATH, date('Ymd'));
		$contents = file_get_contents($file, false);
		$contents .= date('Y/m/d H:i:s'). ' LINEメッセージ送信処理開始'. PHP_EOL;

		// パラメータを指定しないと実行しない
		if (isset($argv[1])) {
			// 引数から指定日数を取得し、メッセージ送信を実施
			$lineObj = new LineAction();
			for ($i = 1; $i < count($argv); $i++) {
				$lineObj->sendMessage($argv[$i]);
			}

			// ログファイル更新して終了
			$contents .= $lineObj->getLogMessage();
			$contents .= date('Y/m/d H:i:s'). ' LINEメッセージ送信処理終了'. PHP_EOL;
		}
		else {
			$contents .= date('Y/m/d H:i:s'). ' パラメータ指定なしのため処理終了'. PHP_EOL;
		}

		file_put_contents($file, $contents);
		exit;
	}
	catch (Exception $e) {
		file_put_contents($file, $e->getMessage());
		exit;
	}
?>