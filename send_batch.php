<?php
	require_once(dirname(__FILE__). '/class/LineAction.php');
	require_once(dirname(__FILE__). '/class/MailAction.php');
	require_once(dirname(__FILE__). '/class/MessageAction.php');
	require_once(dirname(__FILE__). '/config.php');

	try {
		// ログファイル
		$file     = sprintf(SENDMESSAGE_LOGFILE_PATH, date('Ymd'));
		$contents = file_get_contents($file, false);

		// 送信対象メッセージ取得
		$messageObj   = new MessageAction();
		$messageList  = $messageObj->getSendMessageList();
		$messageCount = count($messageList);
		$contents .= date('Y/m/d H:i:s'). ' 送信対象メッセージ件数：'. $messageCount. '件'. PHP_EOL;
		if ($messageCount === 0) {
			$contents .= date('Y/m/d H:i:s'). ' 送信対象メッセージ0件のため処理終了'. PHP_EOL;
			file_put_contents($file, $contents);
			exit;
		}
		foreach ($messageList as $value) {
			// タイトル・メッセージ本文取得
			$id      = $value['id'];
			$title   = $value['title'];
			$message = $value['messages'];

			// 送信中のフラグ設定
			$messageObj->updateSendMessageList($id, SEND_FLG_SENDING);
			$contents .= date('Y/m/d H:i:s'). ' 送信対象メッセージ 送信フラグ更新(送信中)'. PHP_EOL;

			//---------------------
			// 送信内容ログ
			//---------------------
			$contents .= date('Y/m/d H:i:s'). ' ---------------------'. PHP_EOL;
			$contents .= date('Y/m/d H:i:s'). ' メッセージID：'. $id. PHP_EOL;
			$contents .= date('Y/m/d H:i:s'). ' タイトル：'. $title. PHP_EOL;
			$contents .= date('Y/m/d H:i:s'). ' 本文：'. $message. PHP_EOL;
			$contents .= date('Y/m/d H:i:s'). ' ---------------------'. PHP_EOL;
			//---------------------
			// LINEメッセージ送信
			//---------------------
			$contents .= date('Y/m/d H:i:s'). ' LINEメッセージ送信処理開始'. PHP_EOL;
			$lineObj = new LineAction();
			$lineObj->sendInputMessage($message, $title);
			$contents .= $lineObj->getLogMessage();
			$contents .= date('Y/m/d H:i:s'). ' LINEメッセージ送信処理終了'. PHP_EOL;

			//---------------------
			// メール送信
			//---------------------
			$contents .= date('Y/m/d H:i:s'). ' メール送信処理開始'. PHP_EOL;
			$mailObj = new MailAction();
			$mailObj->sendInputMessage($message, $title);
			$contents .= $mailObj->getLogMessage();
			$contents .= date('Y/m/d H:i:s'). ' メール送信処理終了'. PHP_EOL;

			// 送信済のフラグ設定
			$messageObj->updateSendMessageList($id, SEND_FLG_ENDING);
			$contents .= date('Y/m/d H:i:s'). ' 送信対象メッセージ 送信フラグ更新(送信済)'. PHP_EOL;

			file_put_contents($file, $contents);
		}
	}
	catch(Exception $e) {
		file_put_contents($file, $e->getMessage());
	}

?>