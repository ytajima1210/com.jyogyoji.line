<?php
	require_once(dirname(__FILE__). '/../config.php');
	require_once(dirname(__FILE__). '/CommonAction.php');

	/**
	 * LINEでのメッセージ送信クラス
	 * 
	 * @author 		TAJIMA Yoshinori
	 */
	class LineAction extends CommonAction {

		/** ヘッダー情報 */
		private $headers;

		/**
		 * コンストラクタ
		 * HTTPヘッダ情報・グループID取得処理を実施
		 * 
		 * @param		none
		 * @author		TAJIMA Yoshinori
		 */
		public function __construct() {
			// HTTPヘッダを設定
			$this->headers = [
				'Authorization: Bearer ' . LINE_ACCESS_TOKEN,
				'Content-Type: application/json; charset=utf-8',
			];
			// PDOオブジェクト生成
			$this->setDatabaseObject();
		}

		/**
		 * メッセージ送信の実施
		 *
		 * @param 		integer 	$date
		 * @return 		void
		 */
		public function sendMessage($date) {
			// 本文のデータ取得
			$body = $this->getSendMessage($date);
			if (empty($body) === true) {
				return;
			}
			// 送信処理実施
			$this->sendLine($body, MESSAGE_TITLE);
			return;
		}

		/**
		 * LINEの指定グループへのメッセージ送信
		 *
		 * @param		string 		$body
		 * @param		string 		$title
		 * @return 		void
		 * @author		TAJIMA Yoshinori
		 */
		public function sendInputMessage($body, $title) {
			$this->sendLine($body, $title);
		}

		/**
		 * LINEの指定グループへのメッセージ送信
		 *
		 * @param		string 		$body
		 * @param		string 		$title
		 * @return 		void
		 * @author		TAJIMA Yoshinori
		 */
		private function sendLine($body, $title) {
			// BOTへPOSTするメッセージの設定
			$message = $title. PHP_EOL. PHP_EOL. $body;
			// 送信対象LINE ID一覧取得
			$idList = $this->getSendLineId();
			foreach($idList as $id) {
				$post = [
					'to' => $id['line_id'],
					'messages' => [
						[
							'type' => 'text',
							'text' => $message,
						],
					],
				];
				$post = json_encode($post);
				$this->logContents .= date('Y/m/d H:i:s'). '送信対象ID：'. $id['line_id']. PHP_EOL;
				// HTTPリクエストを設定
				$ch = curl_init(LINE_PUSH_API);
				$options = [
					CURLOPT_CUSTOMREQUEST	=> 'POST',
					CURLOPT_HTTPHEADER		=> $this->headers,
					CURLOPT_RETURNTRANSFER	=> true,
					CURLOPT_BINARYTRANSFER	=> true,
					CURLOPT_HEADER			=> true,
					CURLOPT_POSTFIELDS		=> $post,
				];
				curl_setopt_array($ch, $options);
				// 実行
				$result = curl_exec($ch);
				$this->logContents .= date('Y/m/d H:i:s'). '送信結果：'. $result. PHP_EOL;
				curl_close($ch);
				// 1秒スリープ
				sleep(1);
			}
		}

		/**
		 * メッセージ送信対象のLINEID一覧を取得
		 *
		 * @param		none
		 * @return		array
		 */
		private function getSendLineId() {
			// パラメータで指定されたLINE IDが存在するかどうかチェック
			$state = $this->pdoObj->prepare('
				SELECT 
					line_id
				FROM 
					line_id_list 
				ORDER BY 
					id');
			$state->execute();
			return $state->fetchAll();
		}

	}


?>