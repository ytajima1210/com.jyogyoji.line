<?php
	require_once(dirname(__FILE__). '/../config.php');



	/**
	 * LINE/メール送信の共通親クラス
	 */
	class CommonAction {
		
		/** PDOオブジェクト */
		protected $pdoObj;
		/** ログ情報 */
		protected $logContents;

		/**
		 * ログメッセージを取得する
		 * 
		 * @param 		none
		 * @return 		string
		 * 
		 */
		public function getLogMessage() {
			return $this->logContents;
		}

		/**
		 * メッセージ送信の実施
		 *
		 * @param 		integer 	$date
		 * @return 		void
		 */
		protected function getSendMessage($date) {
			// 起動日＋指定日数の予定が存在するかチェック
			$today = date('Y/m/d');
			$state = $this->pdoObj->prepare('
				SELECT 
					names, 
					pattern, 
					duty, 
					meeting,
					DATE_FORMAT(implement_date, "%c") implement_month,
					DATE_FORMAT(implement_date, "%c月%e日") implement_date,
					DATE_FORMAT(implement_time, "%k時") implement_time, 
					DATE_FORMAT(spare_date, "%c月%e日") spare_date
				FROM 
					jyogyoji_plan 
				WHERE 
					:date = implement_date - '. sprintf(PLAN_INTERVAL, $date));
			$state->bindParam(':date', $today, PDO::PARAM_STR);
			$state->execute();
			$data = $state->fetch();

			// 存在しない場合は処理終了
			if ($data === false) {
				$this->logContents .= date('Y/m/d H:i:s'). ' 送信対象データなしのため処理終了'. PHP_EOL;
				return "";
			}

			// 予定ありの場合はメール送信
			$pattern = $data['pattern'];
			$fileName = sprintf(dirname(__FILE__). '/../text/message%d.config', $pattern);
			$this->logContents .= date('Y/m/d H:i:s'). '------送信対象データ--------'. PHP_EOL;
			$this->logContents .= date('Y/m/d H:i:s'). '予定あり　：'. $data['names']. PHP_EOL; 
			$this->logContents .= date('Y/m/d H:i:s'). '日時　　　：'. $data['implement_date']. ' '. $data['implement_time']. PHP_EOL;
			$this->logContents .= date('Y/m/d H:i:s'). 'お当番　　：'. $data['duty']. PHP_EOL;
			$this->logContents .= date('Y/m/d H:i:s'). '会議　　　：'. $data['meeting']. PHP_EOL;
			$this->logContents .= date('Y/m/d H:i:s'). 'パターン　：'. $pattern. PHP_EOL;
			$this->logContents .= date('Y/m/d H:i:s'). 'ファイル名：'. $fileName. PHP_EOL;
			// テキストファイルよりメッセージ取得
			$fp = fopen($fileName, 'r');
			$message = fread($fp, filesize($fileName));
			fclose($fp);
			// メッセージが取得できなかったらメール送信しない
			if (empty($message)) {
				$this->logContents .= date('Y/m/d H:i:s'). ' メッセージ取得できないため処理終了'. PHP_EOL;
				return "";	
			}
			// メッセージに対する引数の設定
			switch($pattern) {
				case 1:
					$body = sprintf($message, $data['implement_date'], $data['implement_time'], $data['names'], $data['duty']);
					break;
				case 2:
					$body = sprintf($message, $data['implement_date'], $data['implement_time'], $data['names'], $data['duty'], $data['meeting']);
					break;
				case 3:
					$body = sprintf($message, $data['implement_date'], $data['implement_time'], $data['names']);
					break;
				case 4: 
					$body = sprintf($message, $data['implement_date'], $data['implement_time'], $data['names'], $data['spare_date']);
					break;
				case 5:
					$body = sprintf($message, $data['implement_date'], $data['implement_time'], $data['names'], $data['duty']);
					break;
				case 99: 
					$body = sprintf($message, $data['implement_date'], $data['implement_time'], $data['names']);
					break;
				default :
					$body = '';
					break;			
			}
			return $body;
		}

		/**
		 * データベース接続用PDOオブジェクトの用意
		 *
		 * @param		none
		 * @return		void
		 */
		protected function setDatabaseObject() {
			// DB設定
			$mysql   = MYSQL_DBNAME;
			$user    = MYSQL_LOGIN_USER;
			$pass    = MYSQL_LOGIN_PASSWORD;
			$options = array(
				PDO::ATTR_ERRMODE             => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_DEFAULT_FETCH_MODE  => PDO::FETCH_ASSOC,
				PDO::ATTR_EMULATE_PREPARES    => false,
				PDO::ATTR_STRINGIFY_FETCHES   => false);
			$this->pdoObj = new PDO($mysql, $user, $pass, $options);
			$this->pdoObj->query('SET NAMES utf8');
		}
	}

?>