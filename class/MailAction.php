<?php
	require_once(dirname(__FILE__). '/../config.php');
	require_once(dirname(__FILE__). '/CommonAction.php');
	require_once("Mail.php");

	/**
	 * LINEでのメッセージ送信クラス
	 * 
	 * @author 		TAJIMA Yoshinori
	 */
	class MailAction extends CommonAction {

		/**
		 * コンストラクタ
		 * HTTPヘッダ情報・グループID取得処理を実施
		 * 
		 * @param		none
		 * @author		TAJIMA Yoshinori
		 */
		public function __construct() {
			// PDOオブジェクト生成
			$this->setDatabaseObject();
		}

		/**
		 * メッセージ送信の実施
		 *
		 * @param 		integer 	$date
		 * @return 		void
		 */
		public function sendMessage($date) {
			// 本文のデータ取得
			$body = $this->getSendMessage($date);
			if (empty($body) === true) {
				return true;
			}
			// 送信処理実施
			$this->sendMail($body, MESSAGE_TITLE);
		}

		/**
		 * 指定メーリングリストへのメッセージ送信
		 *
		 * @param		string 		$body
		 * @param		string 		$title
		 * @return 		void
		 * @author		TAJIMA Yoshinori
		 */
		public function sendInputMessage($body, $title) {
			$this->sendMail($body, $title);
		}

		/**
		 * メール送信
		 *
		 * @param 	string 		$body
		 * @param 	string 		$title
		 * @return 	boolean
		 */
		private function sendMail($body, $title) {
			try {
				$this->logContents .= date('Y/m/d H:i:s'). ' メール送信処理開始'. PHP_EOL;
		
				// SMTPサーバの設定
				$params = array(
					"host" => SMTP_SERVER,
					"port" => SMTP_PORT,
					"auth" => true,
					"username" => SMTP_USER,
					"password" => SMTP_PASSWORD);
				$mailObj = Mail::factory("smtp", $params);
				
				// 送信先メールアドレス設定
				$mailList = $this->getSendMailAddress();
				foreach($mailList as $mail) {
					// 対象アドレスへメール送信
					$recipients = $mail['mail_address'];
					// ヘッダ情報の指定
					$headers = array(
						"To" => $recipients,
						"From" => MAIL_HEADER_ADDRESS,
						"Subject" => mb_encode_mimeheader($title, 'ISO-2022-JP-MS')
					);
					$result = $mailObj->send($recipients, $headers, mb_convert_encoding($body, 'ISO-2022-JP-MS', 'utf-8'));
					$this->logContents .= date('Y/m/d H:i:s'). ' メール送信：'. $recipients. PHP_EOL;
					$this->logContents .= date('Y/m/d H:i:s'). ' メール送信結果：'. print_r($result, true). PHP_EOL;
					// 2秒スリープ
					sleep(2);
				}

				$this->logContents .= date('Y/m/d H:i:s'). ' メール送信処理終了'. PHP_EOL;
				return;
			}
			catch (Exception $e) {
				$this->logContents .= date('Y/m/d H:i:s'). 'メール送信エラー'. PHP_EOL;
				throw $e;
			}
		}

		/**
		 * メッセージ送信対象のメールアドレス一覧を取得
		 *
		 * @param		none
		 * @return		array
		 */
		private function getSendMailAddress() {
			// パラメータで指定されたLINE IDが存在するかどうかチェック
			$state = $this->pdoObj->prepare('
				SELECT 
					mail_address
				FROM 
					mail_data 
				ORDER BY 
					id');
			$state->execute();
			return $state->fetchAll();
		}
	}

?>