<?php
	require_once(dirname(__FILE__). '/../config.php');
	require_once(dirname(__FILE__). '/CommonAction.php');

	/**
	 * 送信するメッセージの取得クラス
	 * 
	 * @author 		TAJIMA Yoshinori
	 */
	class MessageAction extends CommonAction {

		/**
		 * コンストラクタ
		 * PDOオブジェクト取得処理を実施
		 * 
		 * @param		none
		 * @author		TAJIMA Yoshinori
		 */
		public function __construct() {
			// PDOオブジェクト生成
			$this->setDatabaseObject();
		}

		/**
		 * 送信対象メッセージを取得する
		 *
		 * @param 		none
		 * @return 		array
		 */
		public function getSendMessageList() {
			// パラメータで指定されたLINE IDが存在するかどうかチェック
			$state = $this->pdoObj->prepare('
				SELECT 
					id, title, messages
				FROM 
					send_message_list 
				WHERE
					send_flg = 0
				ORDER BY 
					id
			');
			$state->execute();
			return $state->fetchAll();
		}

		/**
		 * メッセージ送信開始/終了日時・送信フラグを更新する
		 *
		 * @param 		integer 	$id
		 * @param  		integer 	$sendFlg
		 * @return 		boolean
		 */
		public function updateSendMessageList($id, $sendFlg) {
			// メッセージ送信の送信フラグ・送信開始/終了日時を更新
			if ($sendFlg === SEND_FLG_SENDING) {
				$state = $this->pdoObj->prepare('
					UPDATE 
						send_message_list 
					SET
						send_flg   = :send_flg,
						send_start = CURRENT_TIMESTAMP(),
						updated_at = CURRENT_TIMESTAMP()
					WHERE
						id = :id 
				');
			}
			else if ($sendFlg === SEND_FLG_ENDING) {
				$state = $this->pdoObj->prepare('
					UPDATE 
						send_message_list 
					SET
						send_flg   = :send_flg,
						send_end   = CURRENT_TIMESTAMP(),
						updated_at = CURRENT_TIMESTAMP()
					WHERE
						id = :id 
				');
			}
			$state->bindParam(':id', $id, PDO::PARAM_INT);
			$state->bindParam(':send_flg', $sendFlg, PDO::PARAM_INT);
			$result = $state->execute();
			return $result;
		}

		/**
		 * 送信対象メッセージを追加する
		 *
		 * @param 	string 		$title
		 * @param 	string 		$messages
		 * @return 	boolean
		 */
		public function insertSendMessageList($title, $messages) {
			$sendFlg = SEND_FLG_NONE;
			
			$state = $this->pdoObj->prepare('
				INSERT  
					send_message_list 
				SET
					title      = :title,
					messages   = :messages,
					send_flg   = :send_flg,
					created_at = CURRENT_TIMESTAMP(),
					updated_at = CURRENT_TIMESTAMP()
			');
			$state->bindParam(':title', $title, PDO::PARAM_STR);
			$state->bindParam(':messages', $messages, PDO::PARAM_STR);
			$state->bindParam(':send_flg', $sendFlg, PDO::PARAM_INT);
			$result = $state->execute();
			return $result;
		}
	}

?>