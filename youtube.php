<!DOCTYPE>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="content-language" content="ja">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>上行寺お知らせ配信</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
</head>
<body>
	<form action="send.php" id="regform" method="post" enctype="application/x-www-form-urlencoded" accept-charset="UTF-8">
		<h1>上行寺Youtube配信 URL通知</h1>
		<div class="form-group">
			<label for="youtube_url" class="ml-1">配信URL</label>
			<input type="text" class="form-control w-75 ml-1" id="youtube_url" name="youtube_url" placeholder="配信URL">
		</div>
		<input type="hidden" id="pattern" name="pattern" value="youtube" />
		<button type="submit" class="btn btn-primary">送信する</button>
	</form>
</body>
</html>