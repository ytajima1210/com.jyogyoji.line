<?php
	require_once(dirname(__FILE__). '/class/LineAction.php');
	require_once(dirname(__FILE__). '/class/MailAction.php');
	require_once(dirname(__FILE__). '/class/MessageAction.php');
	require_once(dirname(__FILE__). '/config.php');

	try {
		// ログファイル
		$file     = sprintf(SENDMESSAGE_LOGFILE_PATH, date('Ymd'));
		$contents = file_get_contents($file, false);

		$contents .= date('Y/m/d H:i:s'). ' 送信対象メッセージ データ登録処理開始'. PHP_EOL;

		// パターン情報取得
		$pattern = $_POST['pattern'];

		if ($pattern === 'message') {
			// フォームの入力内容を取得する
			$message = $_POST['message'];
			$title = $_POST['title'];
		}
		else if ($pattern === 'youtube') {
			// フォームの入力内容を取得する
			$url = $_POST['youtube_url'];
			// テキストファイルよりメッセージ取得
			$fileName = dirname(__FILE__). '/text/message_youtube.config';
			$fp = fopen($fileName, 'r');
			$message = fread($fp, filesize($fileName));
			fclose($fp);
			// メッセージに配信URLを埋め込み
			$message = sprintf($message, $url);
			$title = URL_MESSAGE_TITLE;
		}

		//---------------------
		// 送信内容ログ
		//---------------------
		$contents .= date('Y/m/d H:i:s'). ' ---------------------'. PHP_EOL;
		$contents .= date('Y/m/d H:i:s'). ' タイトル：'. $title. PHP_EOL;
		$contents .= date('Y/m/d H:i:s'). ' 本文：'. $message. PHP_EOL;
		$contents .= date('Y/m/d H:i:s'). ' ---------------------'. PHP_EOL;

		// 送信メッセージ内容を登録
		$messageObj = new MessageAction();
		$messageObj->insertSendMessageList($title, $message);
		$contents .= date('Y/m/d H:i:s'). ' 送信対象メッセージ データ登録処理完了'. PHP_EOL;
		file_put_contents($file, $contents);
	}
	catch(Exception $e) {
		file_put_contents($file, $e->getMessage());
	}

?>
<!DOCTYPE>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="content-language" content="ja">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>送信完了</title>
</head>
<body>
	<h2>LINEメッセージ・メール送信の予約完了しました。</h2>
	<br/>
	<h4>メッセージ送信されるまで5分少々お待ち下さい。</h4>
	<br/>
	<a href="<?php echo $pattern ?>.php">戻る</a>
</body>
</html>